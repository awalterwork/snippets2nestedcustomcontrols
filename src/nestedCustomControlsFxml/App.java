package nestedCustomControlsFxml;

import java.io.IOException;
import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application {

    @Override
    public void start(final Stage stage) {

        final String FXML_FILE = "OutermostFxml.fxml";
        Parent p = null;
        try {
            final URL fxmlResource = this.getClass().getResource(FXML_FILE);
            if (fxmlResource == null) {
                System.out.println(this.getClass().getSimpleName() + " " + FXML_FILE);
            } else {
                final FXMLLoader fxmlLoader = new FXMLLoader(fxmlResource);
                // fxmlLoader.setController(this);

                p = fxmlLoader.load();
            }
        } catch (final IOException e) {
            System.out.println(this.getClass().getSimpleName() + " " + FXML_FILE);
            e.printStackTrace();
        }

        final Scene scene = new Scene(p, 900, 600);

        stage.setScene(scene);
        stage.show();
    }

    public static void main(final String[] args) {
        launch(args);
    }
}
