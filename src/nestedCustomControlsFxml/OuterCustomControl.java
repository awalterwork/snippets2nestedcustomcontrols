package nestedCustomControlsFxml;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

public class OuterCustomControl extends BorderPane {

    @FXML
    private InnerCustomControl innerCustomControl;
    @FXML
    private VBox userSettingsArea;

    public OuterCustomControl() {
        try {
            final FXMLLoader loader = new FXMLLoader(getClass().getResource("OuterCustomControl.fxml"));
            loader.setClassLoader(this.getClass().getClassLoader());
            loader.setController(this);
            loader.setRoot(this);
            loader.load();
        } catch (final IOException exc) {
            exc.printStackTrace();
        }
    }

    public VBox getUserSettingsArea() {
        return userSettingsArea;
    }

    public InnerCustomControl getInnerCustomControl() {
        return innerCustomControl;
    }

}
