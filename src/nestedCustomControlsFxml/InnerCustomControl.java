package nestedCustomControlsFxml;

import java.io.IOException;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

public class InnerCustomControl extends BorderPane {

    @FXML
    private Label screenTitle;
    @FXML
    private HBox mainToolbar;
    @FXML
    private Button backButton;

    public InnerCustomControl() {
        try {
            final FXMLLoader loader = new FXMLLoader(getClass().getResource("InnerCustomControl.fxml"));
            loader.setClassLoader(this.getClass().getClassLoader());
            loader.setController(this);
            loader.setRoot(this);
            loader.load();
        } catch (final IOException exc) {
            exc.printStackTrace();
        }
    }

    public ObservableList<Node> getToolbarContent() {
        return mainToolbar.getChildren();
    }

    public HBox getToolbar() {
        return mainToolbar;
    }

}
